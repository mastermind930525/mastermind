import requests as rq
import json
from itertools import permutations as nPr
nm_url = "https://we6.talentsprint.com/wordle/game/"
register_url = nm_url + "register"
create_url = nm_url + "create"
guess_url = nm_url + "guess"
id = ''
if id == '':
    sesh = rq.Session()
    register_dict = {"mode": "Mastermind", "name": "Mansirat"}
    reg = sesh.post(register_url, json = register_dict)
    id = reg.json()["id"]
    header = {"Content-Type": "application/json"}
    cookies = reg.cookies.get_dict()

if id:
    create_dict = {"id": id, "overwrite": True}
    create = sesh.post(create_url, json = create_dict)
    #print(create.text)

guess = {"id": id, "guess":"hello"}
correct = sesh.post(guess_url,json = guess)
print(correct.text)

words = [word.strip() for word in open("words.txt")]
words2=[]
for word in words:
    if len(set(word)) == 5:
        words2.append(word)
index = 0
mess = correct.json()
print(len(words),len(words2))
while True:
    guess = {"id": id, "guess":words2[index]}
    correct = sesh.post(guess_url,json = guess)
    index+=1
    mess = correct.text
    if correct.json()["feedback"] == 0:
        print(guess.json()["guess"])
        set_guess = set(guess.json()["guess"])
        unaccepted = [word for word in words2 if _ for _ in word in set_guess]
        for i in unaccepted:
            words2.remove(i)
    elif correct.json()["feedback"] == 5:
        final_guess = list(nPr(guess.json()["guess"],5))
        words2 = final_guess

    print(mess,words2[index])
    if "win" in correct.json()["message"]:
        break
print(mess)


