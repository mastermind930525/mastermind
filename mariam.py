#Initializing my  file
import requests as rq
import json
nm_url = "https://we6.talentsprint.com/wordle/game/"
register_url = nm_url + "register"
create_url = nm_url + "create"
guess_url = nm_url + "guess"
id = ''
if id == '':
    sesh = rq.Session()
    register_dict = {"mode": "Mastermind", "name": "Mariam"}
    reg = sesh.post(register_url, json = register_dict)
    id = reg.json()["id"]
    header = {"Content-Type": "application/json"}
    cookies = reg.cookies.get_dict()

if id:
    create_dict = {"id": id, "overwrite": True}
    create = sesh.post(create_url, json = create_dict)
    #print(create.text)

words = [word.strip() for word in open("words.txt")]
words2=[]
for word in words:
    if len(set(word)) == 5:
        words2.append(word)

vowel_checker = ['pills','pulls','polls','palls','pells']
vows_guessed = {}
for i in range(5):
    guess = {"id": id, "guess":vowel_checker[i]}
    correct = sesh.post(guess_url,json = guess)
    vows_guessed[vowel_checker[i][1]] = correct.json()["feedback"]
    print(correct.text)
correct_vows =[]
m = max(vows_guessed.values())
for i in vows_guessed:
    if vows_guessed[i] == m:
        correct_vows.append(i)
words3=[]
print(correct_vows)
incorrect_vows = []
for i in ['a','e','i','o','u']:
    if i not in correct_vows:
        incorrect_vows.append(i)
check = False
for word in words2:
    if all(vow in word for vow in correct_vows):
        if all(vow not in word for vow in incorrect_vows):
            words3.append(word)
print(len(words3))
guess = {"id": id, "guess":words3[0]}
correct = sesh.post(guess_url,json = guess)
mess = correct.text
print(mess,words2[0])
index = 0
mess = correct.json()
minval = len(correct_vows)
print(words3)
while index<=len(words3):
    guess = {"id": id, "guess":words3[index]}
    correct = sesh.post(guess_url,json = guess)
    index+=1
    mess = correct.text
    if correct.json()['feedback'] == minval:
        pass
    print(mess,words3[index])
    feedback = correct.json()['feedback']
    if "win" in correct.json()["message"]:
        break
print(mess)
               